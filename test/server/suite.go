package server

import (
	"context"

	. "github.com/onsi/ginkgo/v2"

	"game-invitation-task/internal/app/api"
)

type Suite struct {
	*api.App
}

func NewSuite() *Suite {
	app, err := api.NewApp()
	if err != nil {
		AbortSuite(err.Error())
	}

	return &Suite{
		App: app,
	}
}

func (s *Suite) Setup(_ context.Context) {
	go func() {
		defer GinkgoRecover()
		var err = <-s.App.Start()
		AbortSuite(err.Error())
	}()

}

func (s *Suite) TearDown(ctx context.Context) {
	if err := s.App.Shutdown(ctx); err != nil {
		AbortSuite(err.Error())
	}
}
