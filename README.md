# Challenge Service

Go Code Challenge

## How to run it?

To run the server, you can use the command below. It runs all dependencies (mongodb, NATS)
and applies mongodb migrations

```bash
make run-api
```

## How to test it?

The codebase contains integration tests stored in `test` folder. It uses Ginkgo as a testing framework helping write
tests in the BDD style.

```bash
make run-it-tests
```
also  use this curl:
```
curl --location 'localhost:8080/api/v1/invitations/twitter-reg1' \
--header 'Content-Type: application/json' \
--data-raw '{
"email": "test1@test.test"
}'
```
## How to contribute? 

### Project structure
The application's structure was inspired by [Clean Architecture](https://github.com/bxcodec/go-clean-arch) (without `usecase` layer) and 
uses [Standard Golang Project Layout](https://github.com/golang-standards/project-layout). 
```
-- api      - openapi specification
-- cmd      - main files
-- config   - config files
-- internal
    -- app      - app initializations
    -- entity   - domain entities, repository interface
    -- http     - implementation of http controllers
        -- invitationservice  - codegenerated server by oapi-codegen
    -- mongodb - mongodb repository
    -- nats    - nats client
-- migrations   - mongodb migrations
-- test     - integration tests
-- third_party  - code related to third_party service
```

### Run locally
You can use the command below to run it using `go` locally

```bash
make run-env
CONFIG_PATH=config/config.local.yaml go run cmd/api/main.go
```

### Used tools
All tools can be installed by the command below
```
make install-go-tools
```

You can use the tools below to:
* [`oapi-codegen`](github.com/deepmap/oapi-codegen) - generate a code and server code from an OpenAPI spec

### Before commit
If you want to add new changes you should use the command below before these changes
```bash
make pre-commit-check
```


