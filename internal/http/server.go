package http

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	middleware "github.com/oapi-codegen/echo-middleware"

	"game-invitation-task/internal/http/invitationservice"
)

func NewServer(cfg Config, herdService *invitationservice.Server) (*http.Server, error) {
	r, err := NewRouter(herdService)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize gin.Engine: %w", err)
	}

	return &http.Server{
		Addr:    cfg.Host,
		Handler: r,
	}, nil
}

func NewRouter(herdService *invitationservice.Server) (*echo.Echo, error) {
	swagger, err := invitationservice.GetSwagger()
	if err != nil {
		return nil, fmt.Errorf("failed to read swagger: %w", err)
	}
	swagger.Servers = nil

	e := echo.New()
	e.Use(echomiddleware.Logger())
	e.Use(echomiddleware.Recover())
	e.Use(middleware.OapiRequestValidator(swagger))

	invitationservice.RegisterHandlers(e, invitationservice.NewStrictHandler(herdService, nil))

	return e, nil
}
