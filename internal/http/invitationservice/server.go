//go:generate oapi-codegen --config=oapi.codegen.yaml ../../../api/api.service.yaml

package invitationservice

import (
	"context"
	"encoding/json"
	"game-invitation-task/internal/nats"
	"log"
	"net/http"
	"time"

	na "github.com/nats-io/nats.go"
)

var _ StrictServerInterface = (*Server)(nil)

type Server struct {
	NatsClient *na.Conn
}

func NewServer(natsClien *na.Conn) *Server {
	return &Server{NatsClient: natsClien}
}

func (s Server) TakeInvitation(ctx context.Context, request TakeInvitationRequestObject) (TakeInvitationResponseObject, error) {

	r := nats.Request{
		Code:  request.Code,
		Email: string(request.Body.Email),
	}
	req, err := json.Marshal(r)
	if err != nil {
		log.Println(r, err)
		return NewErrorResponse(http.StatusBadRequest, "invitation request failed"), nil
	}
	re, err := s.NatsClient.Request("invitation_topic", req, 10*time.Second)
	if err != nil {
		log.Println(r, err)
		return NewErrorResponse(http.StatusBadRequest, "invitation failed"), nil
	}
	resp := nats.Responce{}
	err = json.Unmarshal(re.Data, &resp)
	if err != nil {
		log.Println(resp, err)
		return NewErrorResponse(http.StatusInternalServerError, "invitation failed"), nil
	}

	if resp.Code != http.StatusOK {
		return NewErrorResponse(resp.Code, resp.Error), nil
	}

	return TakeInvitation200Response{}, nil
}

func NewErrorResponse(code int, msg string) TakeInvitationdefaultJSONResponse {
	return TakeInvitationdefaultJSONResponse{
		Body: Error{
			Message: msg,
		},
		StatusCode: code,
	}
}
