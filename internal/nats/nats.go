package nats

import (
	"fmt"
	na "github.com/nats-io/nats.go"
)

type Request struct {
	Code  string
	Email string
}
type Responce struct {
	Code  int
	Error string
}

type Config struct {
	Host string `yaml:"host"`
}

func NewClient(cfg Config) (*na.Conn, error) {
	nc, err := na.Connect(cfg.Host)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize NATS client: %w", err)
	}

	return nc, nil
}
