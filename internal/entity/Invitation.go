package entity

import "go.mongodb.org/mongo-driver/bson/primitive"

type Invitation struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	Name    string             `bson:"name"`
	MaxUses int                `bson:"maxUses"`
	Used    int                `bson:"used"`
	Users   []string           `bson:"users"`
}
