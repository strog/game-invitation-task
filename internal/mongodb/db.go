package mongodb

import (
	"go.mongodb.org/mongo-driver/mongo"
)

func New(db *mongo.Client) *Queries {
	return &Queries{db: db}
}

type Queries struct {
	db *mongo.Client
}
