package mongodb

import (
	"context"
	"game-invitation-task/internal/entity"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
	"testing"
)

// TODO Add more unit tests
func TestInvitationMaxUse(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	email, code := "john.doe@test.com", "testCode"

	mt.Run("success", func(mt *mtest.T) {
		invitationsCollection = mt.Coll
		initation := entity.Invitation{
			Name:    "testCode",
			MaxUses: 1000,
			Used:    1000,
			Users:   make([]string, 0),
		}

		mt.AddMockResponses(mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, bson.D{
			{"_id", initation.ID},
			{"maxUses", initation.MaxUses},
			{"used", initation.Used},
			{"users", initation.Users},
		}))
		err := invitationUpdate(context.Background(), email, code)
		assert.Error(t, err)
	})
}

func TestInvitationUpdate(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	email, code := "john.doe@test.com", "testCode"

	mt.Run("success", func(mt *mtest.T) {
		invitationsCollection = mt.Coll
		initation := entity.Invitation{
			Name:    "testCode",
			MaxUses: 1000,
			Used:    0,
			Users:   make([]string, 0),
		}

		mt.AddMockResponses(mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, bson.D{
			{"_id", initation.ID},
			{"maxUses", initation.MaxUses},
			{"used", initation.Used},
			{"users", initation.Users},
		}))
		mt.AddMockResponses(bson.D{
			{"ok", 1},
			{"value", bson.D{
				{"_id", initation.ID},
				{"maxUses", initation.MaxUses},
				{"used", 1},
				{"users", []string{email}},
			}},
		})
		err := invitationUpdate(context.Background(), email, code)
		assert.Error(t, err)
	})
}
