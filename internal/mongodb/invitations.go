package mongodb

import (
	"context"
	"errors"
	"game-invitation-task/internal/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

var invitationsCollection *mongo.Collection

func (r Repository) InvitationUpdate(ctx context.Context, code, email string) error {
	database := r.q.db.Database("game")
	invitationsCollection = database.Collection("invitations")
	return invitationUpdate(ctx, code, email)
}

func invitationUpdate(ctx context.Context, code, email string) error {
	var invitation entity.Invitation

	filter := bson.M{"name": code}
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := invitationsCollection.FindOne(ctx, filter).Decode(&invitation); err != nil {
		return errors.New("invitation code not found")
	}

	if invitation.Used >= invitation.MaxUses {
		return errors.New("invitation code is exhausted")
	}

	for _, user := range invitation.Users {
		if user == email {
			return errors.New("this email has already used this invitation code")
		}
	}
	update := bson.M{
		"$inc":  bson.M{"used": 1},
		"$push": bson.M{"users": email},
	}

	if _, err := invitationsCollection.UpdateOne(ctx, filter, update); err != nil {
		return errors.New("invitation code was not  update ")
	}
	return nil
}
