package qds

import (
	"context"
	"encoding/json"
	"fmt"
	"game-invitation-task/internal/mongodb"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"os/signal"
	"time"

	"game-invitation-task/internal/nats"
	na "github.com/nats-io/nats.go"
)

// Run creates a new instance of App and runs it
func Run() error {
	log.Println("run qds app")
	app, err := NewApp()
	if err != nil {
		return fmt.Errorf("failed to init app: %w", err)
	}
	return app.Run()
}

// NewApp returns a new instance of App
func NewApp() (*App, error) {
	var app = new(App)
	var err error

	app.Config, err = NewConfig()
	if err != nil {
		return nil, fmt.Errorf("failed to open config: %w", err)
	}

	app.NATSConn, err = nats.NewClient(app.Config.Nats)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize  nats client: %w", err)
	}
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(app.Config.MongoDB))
	if err != nil {
		return nil, fmt.Errorf("mongo client connecting: %w", err)
	}
	app.Repository = mongodb.NewRepository(client)
	return app, nil
}

// App contains all what needs to run the api
type App struct {
	Config     *Config
	Repository *mongodb.Repository
	NATSConn   *na.Conn
}

// Run runs App and waits ending of app life-cycle
func (app *App) Run() error {
	var quit = make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	_, err := app.NATSConn.Subscribe("invitation_topic", func(msg *na.Msg) {
		re := nats.Request{}
		err := json.Unmarshal(msg.Data, &re)
		if err != nil {
			log.Printf("nats start error %v", err)
		}
		err = app.Repository.InvitationUpdate(context.Background(), re.Code, re.Email)

		if err != nil {
			errorResp := nats.Responce{400, err.Error()}
			b, err := json.Marshal(errorResp)
			if err != nil {
				log.Printf("nats resp error %v", err)
			}
			err = msg.Respond(b)
			if err != nil {
				log.Printf("nats resp error %v", err)
			}
		}
		okResp := nats.Responce{Code: 200}
		b, err := json.Marshal(okResp)
		if err != nil {
			log.Printf("nats resp error %v", err)
		}
		err = msg.Respond(b)
		if err != nil {
			log.Printf("nats resp error %v", err)
		}
	})
	if err != nil {
		log.Printf("nats start error %v", err)
		return app.Shutdown(context.TODO())
	}

	<-quit
	log.Println("caught os signal")

	log.Println("trying to shutdown api")

	return app.Shutdown(context.TODO())
}

// Shutdown can be run to clean up all what was run
func (app *App) Shutdown(ctx context.Context) error {
	_, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	app.NATSConn.Close()

	log.Println("api has been shutdown")
	return nil
}
