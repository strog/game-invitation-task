package api

import (
	"context"
	"errors"
	"fmt"
	"game-invitation-task/internal/http/invitationservice"
	"log"
	h "net/http"
	"os"
	"os/signal"
	"time"

	"game-invitation-task/internal/http"
	"game-invitation-task/internal/nats"
	na "github.com/nats-io/nats.go"
)

// Run creates a new instance of App and runs it
func Run() error {
	app, err := NewApp()
	if err != nil {
		return fmt.Errorf("failed to init app: %w", err)
	}
	return app.Run()
}

// NewApp returns a new instance of App
func NewApp() (*App, error) {
	var app = new(App)
	var err error

	app.Config, err = NewConfig()
	if err != nil {
		return nil, fmt.Errorf("failed to open config: %w", err)
	}

	app.NATSConn, err = nats.NewClient(app.Config.Nats)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize  nats client: %w", err)
	}

	invitationService := invitationservice.NewServer(app.NATSConn)
	app.HTTPServer, err = http.NewServer(app.Config.HTTP, invitationService)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize http api: %w", err)
	}

	return app, nil
}

// App contains all what needs to run the api
type App struct {
	Config     *Config
	HTTPServer *h.Server
	NATSConn   *na.Conn
}

// Run runs App and waits ending of app life-cycle
func (app *App) Run() error {
	var errc = app.Start()

	var quit = make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	select {
	case <-quit:
		log.Println("caught os signal")
	case err := <-errc:
		log.Printf("caught error: %s", err)
	}

	log.Println("trying to shutdown api")

	return app.Shutdown(context.TODO())
}

// Start runs App and doesn't wait
func (app *App) Start() <-chan error {
	var errc = make(chan error, 1)

	go func() {
		log.Println("http api has been started")
		if err := app.HTTPServer.ListenAndServe(); err != nil && !errors.Is(err, h.ErrServerClosed) {
			errc <- err
		}
	}()

	return errc
}

// Shutdown can be run to clean up all what was run
func (app *App) Shutdown(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	if err := app.HTTPServer.Shutdown(ctx); err != nil {
		return fmt.Errorf("failed to shutdown http api")
	}

	app.NATSConn.Close()

	log.Println("api has been shutdown")
	return nil
}
