package main

import (
	"log"

	"game-invitation-task/internal/app/api"
)

func main() {
	if err := api.Run(); err != nil {
		log.Fatal(err)
	}
}
