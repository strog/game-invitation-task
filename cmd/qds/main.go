package main

import (
	"log"

	"game-invitation-task/internal/app/qds"
)

func main() {
	if err := qds.Run(); err != nil {
		log.Fatal(err)
	}
}
